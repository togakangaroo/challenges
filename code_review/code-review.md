
# Table of Contents

1.  [Code Review](#org345ff08)
    1.  [Organization](#org1490dbf)
        1.  [High Level organization](#org29e8897)
    2.  [Omnipresent Concerns](#orgcaf8b95):important:
        1.  [Localization](#org6c2ebd7)
        2.  [Break out similar stuff into reusable components](#org395887e)
        3.  [Update React and patterns](#org1af023c)
        4.  [Container vs UI Components](#orgf066e16)
        5.  [Proper usage of semantic HTML](#orga1ef4ef)
    3.  [File Comments](#orgea428f2)
        1.  [`TeamModalLink`](#org2c01672)
        2.  [`TeamModal`](#org7442cc4)
        3.  [`ManageTeam/UserRow`](#org816c062)
        4.  [`AddUser`](#org4ae2923)
        5.  [`ReassignPanel`](#orgd7f1d37)
        6.  [`Elements/UserRow`](#orge270dd1)

Note that not all identified issues are created equal and are tagged with the following tags:

-   `important` - this is a bug, security, or otherwise serious enough issue that it should be addressed
-   `discussion` - this is a part of a broader discussion and might well be broken out into other tasks or even epics
-   `refactoring` - addressing this issue will involve non-trivial refactoring. It could reasonably be a separate tech-debt ticket.

Note that the way gitlab renders `org` files will not show the above tags. Check out the raw document or the exported [adjacent markdown file](./code-review.md).


<a id="org345ff08"></a>

# Code Review


<a id="org1490dbf"></a>

## Organization


<a id="org29e8897"></a>

### High Level organization

Something to consider - I enter this directory and have no idea where to go from there. I would recommend to place components that are used to orchestrate others at the top level. That way, someone exploring the repository has a clear outline of the component flow set out for them and can start by thinking about things at the high level, before moving down to a lower one. Lets build out the dependency diagram (it might be a good idea to scan the AST and build it out automatically in CI).

![img](dependency-tree.png)

Interesting, ok, I think this gives me more insight into what we're doing here.

-   `Elements` seems to be a mostly unorganized grab bag of components. A README in this directory or some further organization in subdirectories might be a good idea.
-   `ManageTeam` This seems to be a component tree with `TeamModalLink` at the high level. Something to indicate this is the upper level might be good.

For this review I will move through the components of `ManageTeam` top-level-down. The earlier component will have more comments as many of them can be applied to latter components as well and should be fixed as a unit. 

I did run out of time while doing this review (Its plenty long as it is!) so realistically we would do it in several sessions. Addressing things higher up in the review would likely modify the comments I would make on things further down the dependency tree anyways.


<a id="orgcaf8b95"></a>

## Omnipresent Concerns     :important:


<a id="org6c2ebd7"></a>

### Localization

There is a lot of hard coded text here. Given what Bridge US does, you'd think that localization would be a high priority and not only is that not being done here but some of these things are coded in a way that would be fairly difficult to localize (for example dynamic content is inserted directly into strings without having dedicated components. You can't do that! Word order is not the same in all languages!

For an example of how localization might work using components provided by React Context see the notes on [`getAccessList` function](#move-getAccessList-to-component)


<a id="org395887e"></a>

### Break out similar stuff into reusable components

There is a lot of common UI and code patterns accross components. These can be moved into custom components. Not only does that build up a common library that you can use throughout the application thereby accelerating development, but it creates an obvious location to address high-level concerns.


<a id="org1af023c"></a>

### Update React and patterns

Several of the lifecycle hooks have been deprecated. A good deal of the state management code here gets simplified by using react hooks properly.


<a id="orgf066e16"></a>

### Container vs UI Components

A common piece of advice in react is to either use react hooks (with lots of custom hooks) or to use the [container](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0) [pattern](https://reactpatterns.com/#container-component) to keep data-management separate from UI definition. This code should probably consider using this pattern.


<a id="orga1ef4ef"></a>

### Proper usage of semantic HTML

This is a very common mistake and not the most impactful, but it does end up mattering for things like accessibility. Most HTML elements have important semantics that are defined in the spec. Elements like `<div>` and `<span>` while they might work visually are typically missing important information and can make the experience significantly worse for people using accessibility tooling (which is not just the disabled - productivity tools often work on similar concepts). It is typically worth the time to read through [tools like the HTML5 table of elements](https://www.madebymike.com.au/demos/html5-periodic-table/) and to keep them front of mind when developing.


<a id="orgea428f2"></a>

## File Comments


<a id="org2c01672"></a>

### `TeamModalLink`

1.  Constructors Are Not Necessary     :refactoring:

    [TeamModalLink.jsx](./ManageTeam/TeamModalLink.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 11:01&gt; </span></span> 
    
        class TeamModalLink extends Component {
         constructor(props) {
           super(props);
           this.state = {
             showModal: false,
           }
         }
    
    I'll point this out here but it really applies to all components everywhere. In modern versions of babel this constructor pattenr is unnecessary, you can simply do
    
        class TeamModalLink extends Component {
          state = {
            showModal: false,
          }

2.  Use React Hooks

    [TeamModalLink.jsx](./ManageTeam/TeamModalLink.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 11:06&gt; </span></span> 
    
        class TeamModalLink extends Component {
    
    Minor point as there's plenty of reasons to do otherwise, but consider using react hooks over class components. They tend to make for significantly simpler components.
    
    Also, consider using the anonymous class form 
    
        const TeamModalLink = class extends Component {
    
    I find this to be significantly more clarifying about whats going on under the hood (it is just a variable that links to that class) with the added benefit of adding `const` checking.

3.  Using <span> and arrows

    [TeamModalLink.jsx](./ManageTeam/TeamModalLink.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 11:12&gt; </span></span> 
    
        render () {
          return (
            <span>
    
    Two things here
    
    1.  Using `<span>`
    
        So not a giant issue but worth considering that this likely violates the HTML spec. `<span>` belongs to a category caled [inline elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Inline_elements) which comes with all sorts of restrictions as far as what can go inside those elements (only other inline stuff). Browser rendering engines dont' really care, but some versions of React might complain and it technically **is** against the spec. I have seen a suggestion in the past to forego `<span>` entirely in favor or the more flexible `<div>` (and just styling as needed) and have been trying that style out for the last year. It works well.
    
    2.  Using property arrow functions
    
        Again not a show-stopper but worth noting that an arrow here will let you save 2 lines of code that is just ceremony
        
            render = () => (
               <span>

4.  `toggleModal` not used

    [TeamModalLink.jsx](./ManageTeam/TeamModalLink.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 11:31&gt; </span></span> 
    
        toggleModal = () => { 
    
    This doesn't seem to be used

5.  `aria-label` contradictory/unnecessary     :important:

    [TeamModalLink.jsx](./ManageTeam/TeamModalLink.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 12:11&gt; </span></span> 
    
        <Button
          aria-label="Open comments"
          onClick={() => this.setState({ showModal: !this.state.showModal })}
          bsStyle="link"
        >
          Edit access
        </Button>
    
    So the point of [`aria-label` is to label an element when another label isn't visible](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-label_attribute). But in this case a label **is** visible. Confusingly it is contradictory. So we probably should remove the `aria-label` and double check if the button contents are correct.

6.  Invert Ternary

    [TeamModalLink.jsx](./ManageTeam/TeamModalLink.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 12:20&gt; </span></span> 
    
        {this.state.showModal ? 
             ...
         :null}
    
    This can be a bit nicer if you invert the ternary. This doesn't require people to scan all the way down to find out what the "otherwise" condition is:
    
        {!this.state.showModal ? null : (
             ...
         )}

7.  Consider moving `TeamModal` into children     :refactoring:

    [TeamModalLink.jsx](./ManageTeam/TeamModalLink.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 12:26&gt; </span></span> 
    
        <TeamModal
          type={this.props.objectType}
          objectName={this.props.objectName}
          objectId={this.props.objectId}
          owner={this.props.owner}
          ownerId={this.props.owner.id}
          currentUserRole={this.props.currentUserRole}
          updateCallback={this.updateOwner}
          updateUsersCallback={this.props.updateUsersCallback}
          matterSlug={this.props.matterSlug}
        />
    
    I notice that this doesn't acutally depend on anything inside of `TeamModalLink`. The only thing the parent component is providing therefore is the fact that it wraps `TeamModal`. Consider simply passing this through as `children`. Then `TeamModalLink` becomes a generic "Modal" component that can be reused elsewhere, doesn't need any props, and you no longer have to deal with the update/mount issue (which is unlikely to be an issue here but not impossible).


<a id="org7442cc4"></a>

### `TeamModal`

1.  Component Too Large

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 12:40&gt; </span></span> 
    This component is just too large. Lets look for an opportunity to break it up smaller

2.  Capitalization not used and move to function

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 12:38&gt; </span></span> 
    
        type: this.props.type.charAt(0).toUpperCase() + this.props.type.slice(1), 
    
    At first I was going to comment on moving this operation to its own function to make it clearer that we're captializing here. But I'm not even sure this is being used at all, is it?

3.  Possible Vulnerability In URL Construction

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 12:50&gt; </span></span> 
    
        Axios({
          method: 'get',
          url: `/access/share_widget.json?object_id=${this.props.objectId}&object_type=${this.props.type.toLowerCase()}`,
    
    It would depend on where `objectId` and `type` come from and what exactly this endpoint does, but since we're just interpolating it into a string, there is a possible vulnerability. The attack would work like this:
    
    -   If attacker **M** can control the `objectId` or `type` set on entities that are created and stored on the backend
    -   And if a victim **V** can pull up a page where this query runs with the values that **M** created
    -   Then **M** can effectively control not only the parameter values in the query triggered from **V**'s browser, but ****which parameters are used****. Say for example that this endpoint took a parameter of `no_audit_trail=1` which left no audit record of the query, but logged that this user was using this parameter possibly flagging them for future investigation. Then **M** could set `objectId` of `whatever&no_audit_trail=1` and **V** would be getting flagged!
    
    Granted, this is an unlikely scenario but worth a double check.

4.  Consider `componentDidUpdate`     :important:

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 13:29&gt; </span></span> 
    
         componentWillReceiveProps = (nextProps) => {
           if (this.state.owner !== nextProps.owner) {
             this.setState({
               owner: nextProps.owner,
             })
           }
        }
    
    This method is [considered legacy](https://reactjs.org/docs/react-component.html#unsafe_componentwillreceiveprops). Possibly a beter idea is to use `componentDidUpdate`? Or even better, a react hook with `useEffect`.
    
    More importantly. There's a bug here by not using the appropriate lifecycle hook. It is possible for the `owner` properties to change yet the component to **not be remounted**. In that case, it will be showing invalid data as it will have been based on the request at mount time.

5.  Introduce `capitalize`     :refactoring:disussion:

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 14:25&gt; </span></span> 
    
        getUserName = (user) => {
          if (user.short_name) {
            return user.short_name.charAt(0).toUpperCase() + user.short_name.slice(1);
          } else if (user.first_name && user.last_name) {
            return (
              user.first_name.charAt(0).toUpperCase() + user.first_name.slice(1)
              + " "
              + user.last_name.charAt(0).toUpperCase() + user.last_name.slice(1)
            )
          } else {
            return user.email;
          }
        }
    
    -   I mentioned earlier, we really should create a helper `capitalize` method rather than doing this over and over
    
    Less important:
    
    -   This doesn't use anything scoped to the component, simply make it a pure helper function or component (bonus is that it can be resusable)
    -   `else` clauses are unecessary as the prior clauses return. The only way those lines of code **would** run is if the previous clauses did not match.
    -   **It is not a valid assumption that any first/last name should be capitalized** eg `Pedro de la Rosa`
    
    Nice potential refactor to a component:
    
        const UserName = ({short_name, first_name, last_name, email}) => (
            short_name                ? capitalize(short_name) :
            (first_name && last_name) ? `${capitalize(first_name)} ${capitalize(last_name)}` :
                                        email
        )
    
    I'll note (though this is probably a larger discussion) that `first_name` and `last_name` are particularly poor fields when dealing with immigrants who simply might not know or even **have** such designations to their names. If possible, simply a `name` field would be preferrable.

6.  `cancel` will not work with multiple components on the page     :important:

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 16:20&gt; </span></span> 
    
        componentWillUnmount = () => {
          // check for cancel token to see if a request is running
          // cleanup and cancel existing requests
          if (cancel !== undefined) {
            cancel();
          }
        }
    
    For one thing `componentWillUnmount = () => cancel && cancel()` - one liner :D
    
    But more importantly, this is a bug as the `cancel` variable is module-level therefore if two instances of this component are mounted on a page (and what reason would some other dev have to think they couldn't be?) then the `cancel` token set on one will overwrite that of another (and it wouldn't get cancelled) but also one getting cancelled will cancel the other even if it is not getting unmounted.
    
    An easy solution would be to scope `cancel` to the component so `this.cancel && this.cancel()`. An even better solution would be `useEffect` and React Hooks.

7.  Create `TeamList` component     :refactoring:

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 14:38&gt; </span></span> 
    
        getTeamList = () => { 
    
    This probably makes more sense as its own component rather than just a function. I'm guessing that this is a function because it is just an array of components and it wasn't clear how to componentize that? The trick is to map into a `React.Fragment` or it's more modern form `<>`.
    
    Also, a potential bug here is that if there is an error but **not** users, it will not show an error.
    
    If `users` are mapped over often but in different ways then consider something like this.
    
        const TeamList = ({users, hasError, type, renderUser}) => {
            if(hasError)
                return <p className="lead text-center">We were unable to retrieve the members of this {type}.</p>
            if(!users || !users.length)
                return
            return React.createElement(React.Fragment, null, users.map(renderUser))
        }

8.  `getAccessList` function     :discussion:

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 15:44&gt; </span></span> 
    
    So I don't want to be tearing apart minutea here but its worth pointing out how bizarre this method is
    
        getAccessList = (users) => {
          let inheritedUsers = '';
        
          users.map((user, index) => {
            switch (true) {
              case index === 0:
                inheritedUsers = this.getUserName(user);
                break;
              case index === users.length - 1:
                inheritedUsers = `${inheritedUsers} and ${this.getUserName(user)}`;
                break;
              default:
                inheritedUsers = `${inheritedUsers}, ${this.getUserName(user)}`;
                break;
            }
          });
        
          inheritedUsers = <span><span className="bold-text">{inheritedUsers}</span> {users.length > 1 ? 'have' : 'has'}</span>
        
          return inheritedUsers;
        }
    
    -   this is a use of `map` to do what seems to be a natural `reduce` operation
    -   the name doesn't seem to match what we're doing here - you're simply outputing a list of usernames
    -   as before this seems a natural use case for a custom component
    -   the usage of a `switch` here&#x2026;I'm not sure what the benefit is over an `if...else`, it just adds ceremony with the need for `break;`
    -   Also I would recommend considering placing the commas and `and` separators (btw - use an oxford comma!) with css pseudoelements rather than in the html directly. This is arguably a matter of preference but it seems to me like seperators are inherently styling. In fact, because these rules are going to vary based on culture, it would possibly be a good idea to make it its own component that can be internationalized.
    
    Here's another potential approach without the same limitations:
    
        const useLocalizer = () => useContext(Localizer) //this would be placed in a file
        
        const el = React.createElement
        const UsersHaveAccess = ({users, permission}) => {
            const l = useLocalizer()
            // Note the version of babel typically used with react doesn't support spreading into jsx (even though jsx itself does)
            const localizedUserList = el(l.ListOfItems, null, ...users.map(user => el(UserName, {user})))
            return (
                <aside>
                  {localizedUserList}
                  <l.PosessiveFor items={users} />
                  <l.Permission permission={permission} />
                </aside>
            )
        }

9.  Consider Using `async` function for getting data

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 16:18&gt; </span></span> 
    consider using `async/await` here
    
        }).then((response) => {

10. Consider wrapping `Axios`

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 16:39&gt; </span></span> 
    
        Axios({
    
    Consider wrapping all references to Axios. For one thing it will give you a place to put things like common interceptors. For another it will let you do things that really should affect **all** requests like for example unwrapping `request.data` (so you don't have to do it all the time). And automatically `camelCaseing` all properties on `data`.

11. Opportunity to use destructuring and more     :discussion:

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 16:29&gt; </span></span> 
    
        then((response) => {
          let primaryUsers = response.data.users.primary.edit.concat(response.data.users.primary.view);
        
          this.setState({
            currentUserPermission: response.data.current_user_permission,
            primaryUsers: primaryUsers,
            inheritedUsers: response.data.users.inherited,
            eligibleUsers: response.data.users.all_users,
            hasError: false,
            matterName: response.data.users.inherited.matter.name ? response.data.users.inherited.matter.name : 'the matter',
          });
    
    Not a big deal at all but there's an opportunity here to use some es6 features to simplify and make it clearer what we expect back
    
        then(({data: {users, current_user_permission}) => {
          const primaryUsers = [users.primary.edit...users.primary.view];
        
          this.setState({
            primaryUsers,
            currentUserPermission: current_user_permission,
            inheritedUsers: users.inherited,
            eligibleUsers: rusers.all_users,
            hasError: false,
            matterName: users.inherited.matter.name ? users.inherited.matter.name : 'the matter',
          });
    
    Also again, opportunity for localization.
    
    Finally a discussion topic - I am confused by what `inheritedUsers` is - it sounds plural so I would expect it to be an aray, but `users.inherited` has a `matter` property, so it can't be an array. You can see my confusion. Perhaps something needs to be named or structured differently?

12. `updateCallback` is confusing     :discussion:

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 19:27&gt; </span></span> 
    
        if (owner) {
          // Update owner in parent component (widget)
          this.props.updateCallback(owner, successMessage);
        }
    
    So I find this very confusing. The `owner` variable isn't even used in firing the query but if it is passed in then we're going to wait till the query finishes and then trigger a generic `updateCallback`?
    
    I'm not really saying its wrong, I just really don't understand it and its worth a conversation as to what is going on here, why it is needed, and whether it can be refactored to be less confusing.
    
    **Suggestion:**
    It looks like the only time `owner` is even passed in is [here](ManageTeam/TeamModal.jsx)
    
        updateData = (owner, successMessage) => {
          // Update team list in this modal
          this.getData(owner, successMessage);
        }
    
    If `getData()` returns a promise, then you can just wait until it succeeded and then call `updateCallback` here.

13. Move Modal-specific stuff up to `TeamModalList`

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 20:46&gt; </span></span> 
    
        <Modal.Header closeButton>
          <Modal.Title>Access to <span className="bold-text">{this.props.objectName}</span></Modal.Title>
        </Modal.Header>
        <Modal.Body>
    
    I [mentioned](#moving-TeamModal-into-children) that `TeamModalList` can become a more generic App-specific Modal component. If that is the case then all this modal-specific stuff should bump up to that component and let only the basic structure here remain

14. Pull Calculation of Permissions Into Dedicated Function

    [TeamModal.jsx](./ManageTeam/TeamModal.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 21:05&gt; </span></span> 
    
        this.state.inheritedUsers.matter
                       && this.state.inheritedUsers.matter !== null
                       && Object.keys(this.state.inheritedUsers.matter).length !== 0
                       && this.state.inheritedUsers.matter.edit.length > 0 ? 
    
    It might make sense to pull out all the calculation of permissions into a single function that just does all the calculation at once and returns a single object from which access types are available. Between that and [moving `getAccessList` to a component](#move-getAccessList-to-component), I believe this will get drastically simpler.


<a id="org816c062"></a>

### `ManageTeam/UserRow`

1.  Avoid pushing props to state

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 21:27&gt; </span></span> 
    
        this.state = {
          permissionLevel: this.props.user.permission,
          accessMessage: this.props.user.access_message,
    
    This is [considered a react anti-pattern](https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html). For one thing you will be an invalid state if `props.user` changes out from under you without the component remounting.

2.  `cancelUpdate` is scoped to module not component     :refactoring:

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 21:48&gt; </span></span> 
    
        componentWillUnmount = () => {
          // check for cancel token to see if a request is running
          // cleanup and cancel existing requests
          if (cancelUpdate != undefined) {
            cancelUpdate();
          }
        
          if (cancelRemove != undefined) {
            cancelRemove();
          }
        
          // check for timeout running and cleanup any existing
          if (this.timeout) {
            clearTimeout(this.timeout);
          }
        }
    
    `cancelUpdate` and `cnacelRemove` are module scope. As mentioned [previously](#cancel-will-not-work-with-multiple-components-on-page), this may cause problems when there are multiple instances of this component on the page (which this being a User Row, I would assume there would be).
    
    Also
    
        cancelUpdate && cancelUpdate()
        cancelRemove && cancelRemove()
        clearTimeout(this.timeout)
    
    That last line doesn't need an `if` check.

3.  `removeUser`     :refactoring:

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 22:28&gt; </span></span> 
    
        removeUser = (event) => {
           if (event) {
             event.preventDefault()
           };
        
           var token = "null";
           if (document != undefined && document.querySelector('meta[name=csrf-token]') != null) {
             token = document.querySelector('meta[name=csrf-token]').getAttribute('content');
           }
    
    -   `removeUser` is never called with `event` so those 4 lines are not needed
    -   avoid using `var`, there isn't really a need for that anymore
    -   This is out of place in this component as the details of how to fetch a meta field are a completely separate responsibility. It can be moved to a separate module or referenced via a library like [react-helmet](https://www.npmjs.com/package/react-helmet).
    -   I'll note that searching for a meta attribute is not really the correct way of doing this, meta attributes are (I believe) meant to be just another way of setting a header field via content, but if you moved this to a header, this `querySelector` would fail.
    -   not sure why we would ever set it to the **string** `null` either. What is the idea there?

4.  `componentWillRecieveProps`

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 22:55&gt; </span></span> 
    
        componentWillReceiveProps = (nextProps) => { 
    
    I [mentioned it above](#teammodal-componentWillRecieveProps), but this life cycle hook is deprecated

5.  Simplified `then` callbacks

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 22:56&gt; </span></span> 
    
        .then(() => {
          // run callback to update users and remove this from display
          if (this.props.updateUsersCallback) {
            this.props.updateUsersCallback();
          }
        
          cancelRemove = undefined;
        }).catch((err) => {
          cancelRemove = undefined;
          console.log(err);
        });
    
    So long as `updateUsersCallback` is bound properly you could do
    
        .then(this.props.updateUsersCallback)
        .catch(() => console.log(err))
        .finally(() => cancelRemove = undefined);

6.  Incorrect derived state modification

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 22:59&gt; </span></span> 
    
        let isOpen = this.state.isPanelOpen;
        this.setState({
          isPanelOpen: !isOpen,
        });
    
    [Modifying state from existing state is an explicitly listed anti-pattern](https://reactjs.org/docs/react-component.html#setstate). The correct thing to do is the callback form
    
        this.setState(({isPanelOpen}) => ({isPanelOpen: !isPanelOpen}))
    
    It looks dumb, that without that state baching can cause bugs as described in the last few paragaraphs of the linked documentation.

7.  `handlePermissionChange`     :refactoring:

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 23:04&gt; </span></span> 
    
        handlePermissionChange = (event) => { 
    
    Same comments as above for `removeUser` with 2 additions.
    
    -   The fact that the method names follow an inconsistent pattern, it is confusing given that the two functions do very similar things, maybe think on a naming style to use? My rule of thumb is to name functions after what they do, not how they're used.
    -   We should probably abstract all this common code into a helper function.

8.  Create `FlashMesage` comonent     :refactoring:

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-05 Tue 23:07&gt; </span></span> 
    
        this.setState({
          accessMessage: response.data.access_message,
          successDisplay: true,
        });
        
        this.timeout = window.setTimeout(() => {
          this.setState({
            successDisplay: false,
          });
        
          this.timeout = null;
        }, 5000);
    
    This seems to be entirely for the purpose of flashing a message to the screen. This should probably just go into its own component 
    
        <FlashMessage message={access_message} duration={5000} />
    
    also no need to clear out `this.timeout`.

9.  Consider refactoring getUserName     :disussion:

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 09:18&gt; </span></span> 
    
        getUserName = (user) => {
          if (user) {
            if (user.full_name) {
              return(
                <div>
                <h4 className="user-title">{`${user.full_name}`}</h4>
                <p className="text-muted">{user.email}</p>
                </div>
              );
            } else if (user.short_name) {
              return(
                <div>
                <h4 className="user-title">{`${user.short_name}`}</h4>
                <p className="text-muted">{user.email}</p>
                </div>
              );
            } else {
              return <h4 className="user-title">{`${this.state.type} member`}</h4>;
            }
          } else {
            return <h4 className="user-title">{`${this.state.type} member`}</h4>;
          }
        }  
    
    This seems to at the same time be similar to the [need for a generic `UserName` component we discussed earlier](#teammodal-getUserName), but also some of the logic is different. For example previously we were showing a name by assembling first and last, but here we see that a `user.full_name` has been available the whole time? We probably should have a discussion over how to display names overall and use a common component
    
    A few other more minor notes 
    
    -   Indentation on jsx is off
    -   This string interpolation is redundant: ``{`${user.short_name}`}``
    -   Are you sure that `<div>` is the right element here? This feels more like a case for a `<section>` or maybe an `<aside>`
    -   Is `<h4>` being used correctly here? These elements aren't really for headings and certainly not just for styling, they're meant to represent an outline of the outline of the section and [come with plenty of cautions on usage](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements#Usage_notes). If all you want is a heading semantic within a section then the `<header>` element is appropriate.

10. `getPermission`     :disussion:

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 09:27&gt; </span></span> 
    
        getPermission 
    
    I think it might be a good idea to rename this - reading the method I immediately see that it returns not really a permission but a boolean representing whether a permission is granted. So the question is immediately "permission for **what**"? It might be a good idea to just include that in the function name.
    
    I also note all the focus on roles means that the permission system might be role based rather than permission-based? The latter is generally much for flexible and it might be worth a broader conversation on how permissions are handled.
    
    Minor suggestions, whether you take them is up to you:
    
    -   With a good name this can easily be a free-floating function in its own module which serves to both make it reusable and to clean up this component
    -   Any `else` statements following a `return` are redundant as the only way you're getting to that line of code is if the condition didn't pass.
    -   This could easily be a single `return` with just a set of `||` statements. You can even keep the comments and it collapses down to one-line-per-condition rather than three.

11. Consider a `HasPermission` Component

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 09:34&gt; </span></span> 
    
        {this.props.currentUserPermission === "edit" && this.getPermission() ?
          <Button
            bsStyle="link"
            onClick={this.toggleReassignPanel}
            id={`remove-user-${this.props.user.id}`}
            className="remove-link pull-right"
          >
            Reassign {this.props.objectType}
          </Button>
        : null }
    
    Rather than doing this over and over (and having the previously discussed `getPermission` block), how about a component which handles all that for you?
    
        <HasPermission requires="edit" currentRole={this.props.currentUserRole} userRole={this.props.user.role} render={() => (
           <Button bsStyle="link" onClick={this.toggleReassignPanel}
             id={`remove-user-${this.props.user.id}`} className="remove-link pull-right">
             Reassign {this.props.objectType}
           </Button>
        )} />
    
    -   Also I'm not sure of the purpsoe of having an `id` here, I don't really see anything using it.
    -   Also - as noted before - use of inline text like this is going to make localization difficult

12. Notes on selector

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 09:43&gt; </span></span> 
    
        <FormControl
          componentClass="select"
          placeholder="select"
          onChange={this.handlePermissionChange}
          value={this.state.permissionLevel}
        >
          <option
            className="access_level"
            value="edit"
          >
            Can edit
          </option>
        
          <option
            className="access_level"
            value="view"
          >
            Can view
          </option>
        </FormControl>
    
    -   If you're going to be generating selectors manually every time you need them you're in for a bad time, just write a custom `<Select>` component that allows you to pass in a json object of `[{value, label, {otherProps}}]` and standardizes what selectors look like.
    -   This is no accessible. There's places elsewhere that we go so far as to use `aria-label` but here for a permission control, the only hint a vision-limited person gets is the placeholder `"select"` :P. Use a better placeholder that describes what they are selecting.
    -   Also I'll note that for two options, a better control might be a checkbox, or maaybe a radio button.

13. Another `<h4>`

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 09:48&gt; </span></span> 
    
        <h4>{`Can ${this.state.permissionLevel}`}</h4>
    
    -   This doesn't feel like a proper use of `<h4>` as in it is the fourth nesting level within the section's outline. Maybe I'm wrong?
    -   Again, think of how localization for something like this could work&#x2026;

14. Another place where you seem to want a `capitalize` function

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 09:51&gt; </span></span> 
    
        accessMessage = this.state.accessMessage.charAt(0).toUpperCase() + this.state.accessMessage.slice(1);
    
    Also is this really necessary? This is something that comes from the server, right? Is the server improperly casing stuff?

15. Simplify `Avatar` interface?

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 09:53&gt; </span></span> 
    
        <Avatar
          user={this.props.user}
          thumbnail={this.props.user.thumbnail}
        />
    
    consider making it possible to omit `thumbnail` as a prop and have the `Avatar` draw it out of the `user` if its available as a property on there

16. Use proper sectioning content

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 09:54&gt; </span></span> 
    
        <div className="team-member">
          <div className="row">
            <div className="col-xs-2 col-md-1">
    
    Use proper sectioning content elements. This feels like a pretty clear case for `<section>` (which would make all those internal headers into `h1` if they're even meant to be headers at all).
    
    I suspect that the `UserRow` component is named this simply because it is styled as a `row` here. It might be worth considering renaming it for what it actually displays and does, rather than the style we are choosing at the moment (ie, it would be weird if we decide later it is better as a tile-based-ui but then its still called `UserRow`)

17. Consider being more specific on the shape of `user` prop

    [UserRow.jsx](./ManageTeam/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 10:13&gt; </span></span> 
    
        user: PropTypes.object.isRequired,
    
    I'm not a huge fan of `PropTypes` but if you're going to be doing this, I'll note that some properties of `user` have to be there, some don't. Consider using the `shape` feature to specify which specific properties you are interested in. Potentially that `PropType` definition can even be moved into its own module if it is reused elsewhere.


<a id="org4ae2923"></a>

### `AddUser`

1.  Don't use regex for email validation

    [AddUser.jsx](./ManageTeam/AddUser.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 10:43&gt; </span></span> 
    
        // Check if an access level is selected and an email with valid format is entered
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    [Don't use regex](https://davidcel.is/posts/stop-validating-email-addresses-with-regex/) [for this](https://medium.com/hackernoon/the-100-correct-way-to-validate-email-addresses-7c4818f24643). I'll also note that this fails to validate a perfectly legitimate **intranet** email address lke `george@foo` (note no tld)

2.  Consider adding more info on validation

    [AddUser.jsx](./ManageTeam/AddUser.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 10:45&gt; </span></span> 
    
        if (inputType == "email") {
          let email = value;
          if (email.length < 6 || !this.state.accessLevel) {
            // invalid email if doesn't even have min number of chars
            return false;
          } else {
            return (re.test(email) && this.state.accessLevel.length > 0);
          }
        } else {
          let accessLevel = value;
          if (accessLevel && this.state.value && re.test(this.state.value)) {
            return true;
          } else {
            return false;
          }
        }  
    
    I'll note that there are several reasons valication might fail but we're not noting way and simply reporting a boolean. Personally, I find UIs that will tell me my input is invalid but won't tell me **why** it is invalid to be infuriating. Consider instead of returning a boolean, setting state to a list of error strings - if that list is empty then there are no errors!

3.  Previous notes apply     :disussion:refactoring:important:

    For example about constructors, prop-derived state, Azios, module-scope cancel tokens, proper `select` usage, localization, etc.
    
    Also I'll note that there is a lot of code shared here with `UserRow` suggesting that there is a range of reusable components worth refactoring.

4.  Be careful with too many ids

    [AddUser.jsx](./ManageTeam/AddUser.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 11:20&gt; </span></span> 
    
        <Button
          type="submit"
          className="btn-auto"
          onClick={this.handleSubmit}
          id="grant_access"
          disabled={this.state.isValid ? false : true}
        >
          Grant access
        </Button>
    
    Be careful with using element ids where you don't have to. It is techinically against the spec to have two instances of the same id present on a page. Though browsers seem to handle it fine, it is best to avoid an id if not strictly necessary for an internal link, or a `<label>` target or something.
    
    Also `{this.state.isValid ? false : true}` can just be `{!this.state.isValid}`

5.  Check `ControlLabel` usage

    [AddUser.jsx](./ManageTeam/AddUser.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 11:27&gt; </span></span> 
    
        <ControlLabel>Email address</ControlLabel> 
    
    I'm not sure as I generally eschew boostrap (thats a whole other rant), but I don't think this is correct usage of `ControlLabel`. I believe this generates a styled `<label>` element, but the entire point of that element is that it is bonded to a single form control. There are two ways to do that - either via nesting (preferred) or the `for` attribute pointing to an id. We're not doing either here

6.  Consider creating an `Autosuggest` wrapper component

    [AddUser.jsx](./ManageTeam/AddUser.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 11:31&gt; </span></span> 
    
        <div className="autosuggest-wrapper">
          <Autosuggest
            suggestions={suggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            inputProps={inputProps}
            theme={inputStyle}
          />
        </div>
    
    There's an awful lot of configuration happening with `Autosuggest`. Maybe just create a dedicated component to do that and pull it out of here. Maybe even create a version where you can just pass it an api method and it does all the loading and updating of data on its own
    
    In general, I find for anything but the most simple 3rd party component it is a good idea to wrap them in your own version, even if you're doing nothing more than pasisng through props, it gives you the hooks to really build out cross-application features down the line and does so basically for free.


<a id="orgd7f1d37"></a>

### `ReassignPanel`

1.  Previoius notes apply     :disussion:refactoring:important:

    [ReassignPanel.jsx](./ManageTeam/ReassignPanel.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 11:47&gt; </span></span> 
    
    Many of the points brought up earlier apply here.

2.  `removeOwner` simplification

    [ReassignPanel.jsx](./ManageTeam/ReassignPanel.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 11:49&gt; </span></span> 
    
        removeOwner = () => {
          let users = [];
          for(let i = 0; i < this.state.eligibleUsers.length; i++)
          {
            if(this.state.eligibleUsers[i].id != this.state.ownerId)
            {
              users.push(this.state.eligibleUsers[i]);
            }
          }
          return users;
        }
    
    can just be
    
        removeOwner = () => this.state.eligibleUsers.filter(u => u.id != this.state.ownerId)

3.  Introducing a Modal wrapper component would simplify this

    [ReassignPanel.jsx](./ManageTeam/ReassignPanel.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 11:51&gt; </span></span> 
    
        <Modal
          show={this.state.isPanelOpen}
          onHide={this.clearForm}
          backdropClassName="dark-modal-backdrop"
          dialogClassName="bridge-modal reassign-panel"
        >
    
    Note that [introducing a custom modal component](#moving-TeamModal-into-children) as previously suggested would significantly simplify this


<a id="orge270dd1"></a>

### `Elements/UserRow`

[UserRow.jsx](./Elements/UserRow.jsx) <span class="timestamp-wrapper"><span class="timestamp">&lt;2019-11-06 Wed 12:06&gt; </span></span> 

This component is either not used at all or is used but probably needs a different name. It has the same name as the `ManageTeam` version but does drastically different things

