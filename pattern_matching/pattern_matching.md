
# Table of Contents

1.  [What is this?](#org331e04e)
2.  [Pattern Matching](#org241698c)
    1.  [Thinking it through](#org83dc679)
        1.  [Ruby Playground](#orgab3f8a0)
        2.  [Solution](#org5de7bed)
        3.  [Tests and Files](#org1c0bf04)


<a id="org331e04e"></a>

# What is this?

This is a [literate programming](https://en.wikipedia.org/wiki/Literate_programming) file written in [org mode](https://orgmode.org/). It is my favorite way to explore and ultimately code small problems. Think of [Jupyter Notebooks](https://jupyter.org/) on steroids. The code fragments below are not copy pasted into the document, they are actually executed by the document itself and the results are inlined. A separate operation (called tangling) then pulls out the code blocks into their own files which can be executed as normal.


<a id="org241698c"></a>

# Pattern Matching

See [here](./pattern_matching) for instructions.

<table id="org540ac2f" border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">pattern</th>
<th scope="col" class="org-left">input</th>
<th scope="col" class="org-left">result</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">abab</td>
<td class="org-left">redblueredblue</td>
<td class="org-left">true</td>
</tr>


<tr>
<td class="org-left">aaaa</td>
<td class="org-left">asdasdasdasd</td>
<td class="org-left">true</td>
</tr>


<tr>
<td class="org-left">aabb</td>
<td class="org-left">xyzabcxyzabc</td>
<td class="org-left">false</td>
</tr>
</tbody>
</table>


<a id="org83dc679"></a>

## Thinking it through

Ok, so the obvious solution is to build a state machine that works similarly to regex for matching states and to do it that way.

However! Little known fact. Most regex can reference groups within the pattern itself. So something like `/(.+)(.+)\1\2/` would match `abab`. Therefore, rather than reimplementing regex, can I just use regex? Yes, I can!

It does feel kinda like cheating but my entire selling point is that I'm senior and know shortcuts like this isn't it?


<a id="orgab3f8a0"></a>

### Ruby Playground

So I don't know Ruby super well. I know the basics. This section is about me figuring out how to do things with Ruby. I could omit it from my export but I feel like its good to show my work. You can just skip to the [2.1.2](#org5de7bed)

Lets figure out if Ruby's regex can work this way.

    abab = /^(.+)(.+)\1\2$/
    [
      "abab".match(abab),
      "redblueredblue".match(abab),
      "foo".match(abab),
      "cababc".match(abab),
    ].map {|x| !!x}

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">true</td>
<td class="org-left">true</td>
<td class="org-left">false</td>
<td class="org-left">false</td>
</tr>
</tbody>
</table>

Well that is promising.

What about creating a regex from a string?

    !!Regexp.new("^(.+)(.+)\\1\\2$").match("redblueredblue")

    true

alright then&#x2026;just need those double slashes. This seems simple enough

One more thing, I am going to need work with a hash, let's figure out some basic operations here

    h = {}
    h["a"] = h.length + 1
    h["b"] = h.length + 1
    p h

    {"a"=>1, "b"=>2}

Alright then, I think we're good to get started.


<a id="org5de7bed"></a>

### Solution

So I'm thinking we can just move through the pattern, convert the first occurrence of each letter to `(.+)` and assign it an incrementing number starting with `1`. Then for each subsequent occurrence of that letter output `\\N` instead where `N` is its number.

This sounds like a job for a generator (Disclaimer: I always think things sound like a job for generators)

    def pattern_to_regex_pieces(pattern)
      Enumerator.new do |e| 
        e.yield "^"
        char_nums = {}
        pattern.chars.each do |c| 
          if !char_nums.key?(c)
            char_nums[c] = char_nums.length + 1
            e.yield "(.+)"
          else
            e.yield "\\#{char_nums[c]}"
          end
        end
        e.yield "$"
      end
    end

Now we just create a pattern matcher class that takes the above stream of strings and puts it together into a regex which can be matched on.

    PatternMatcher = Class.new do
      def matches?(text)
        !!@rx.match(text)
      end
    
      def initialize(pattern)
        @rx = Regexp.new(pattern_to_regex_pieces(pattern).reduce(&:+))
      end
    
      def pattern_to_regex_pieces(pattern)
        Enumerator.new do |e| 
          e.yield "^"
          char_nums = {}
          pattern.chars.each do |c| 
            if !char_nums.key?(c)
              char_nums[c] = char_nums.length + 1
              e.yield "(.+)"
            else
              e.yield "\\#{char_nums[c]}"
            end
          end
          e.yield "$"
        end
      end
    
    end

I think that should do it.

    examples.map do |(pattern, input, result_str)| 
      result = PatternMatcher.new(pattern).matches?(input)
      expected = result_str == "true"
      "#{result == expected ? '✓' : ''} Pattern: #{pattern}, Input: #{input}, Result: #{result}"
    end

-   ✓ Pattern: abab, Input: redblueredblue, Result: true
-   ✓ Pattern: aaaa, Input: asdasdasdasd, Result: true
-   ✓ Pattern: aabb, Input: xyzabcxyzabc, Result: false


<a id="org1c0bf04"></a>

### Tests and Files

-   [Pattern Matcher Ruby Class](tangle/pattern-matcher.rb)
-   [Tests](tangle/pattern-matcher-tests.rb)

I know that Ruby is all about metaprogramming so I figure theres a way to auto-generate the test methods themselves. I [Googled around a bit for this](https://stackoverflow.com/a/38233191/5056) and it looks relatively straight forward. I'm sure this isn't exactly idomatic but it works fine.

    require "./pattern-matcher.rb"
    require "test/unit"
    
    class TestSimpleNumber < Test::Unit::TestCase
      DATA.each do |(pattern, input, expected_str)| 
        define_method :"test_pattern_#{pattern}_with_input_#{input}" do
          matcher = PatternMatcher.new(pattern)
          expected = expected_str == "true"
          assert_equal(expected, matcher.matches?(input))
        end
      end
    end

So now lets just run tests

    ruby pattern-matcher-tests.rb | tail -n -4

    3 tests, 3 assertions, 0 failures, 0 errors, 0 pendings, 0 omissions, 0 notifications
    100% passed
    ------
    4081.63 tests/s, 4081.63 assertions/s

Et voila!

