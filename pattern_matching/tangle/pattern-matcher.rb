PatternMatcher = Class.new do
  def matches?(text)
    !!@rx.match(text)
  end

  def initialize(pattern)
    @rx = Regexp.new(pattern_to_regex_pieces(pattern).reduce(&:+))
  end

  def pattern_to_regex_pieces(pattern)
    Enumerator.new do |e| 
      e.yield "^"
      char_nums = {}
      pattern.chars.each do |c| 
        if !char_nums.key?(c)
          char_nums[c] = char_nums.length + 1
          e.yield "(.+)"
        else
          e.yield "\\#{char_nums[c]}"
        end
      end
      e.yield "$"
    end
  end

end
