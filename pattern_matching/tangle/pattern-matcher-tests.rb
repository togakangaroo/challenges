DATA=[["abab", "redblueredblue", "true"], ["aaaa", "asdasdasdasd", "true"], ["aabb", "xyzabcxyzabc", "false"]]
require "./pattern-matcher.rb"
require "test/unit"

class TestSimpleNumber < Test::Unit::TestCase
  DATA.each do |(pattern, input, expected_str)| 
    define_method :"test_pattern_#{pattern}_with_input_#{input}" do
      matcher = PatternMatcher.new(pattern)
      expected = expected_str == "true"
      assert_equal(expected, matcher.matches?(input))
    end
  end
end
